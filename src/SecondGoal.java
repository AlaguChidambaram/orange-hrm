import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SecondGoal {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		// URL
		System.setProperty("webdriver.chrome.driver", "C:\\Work\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://opensource-demo.orangehrmlive.com/index.php/pim/viewEmployeeList");
		driver.manage().window().maximize();

		// LOGIN
		driver.findElement(By.id("txtUsername")).sendKeys("Admin");
		driver.findElement(By.id("txtPassword")).sendKeys("admin123");
		driver.findElement(By.xpath("//input[@type='submit']")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		
		//Leave
		WebElement leave=driver.findElement(By.xpath("//*[@id='menu_leave_viewLeaveModule']"));
		leave.click();
		driver.findElement(By.xpath("//*[@id='menu_leave_assignLeave']")).click();
		WebElement name=driver.findElement(By.name("assignleave[txtEmployee][empName]"));
		name.sendKeys("Ja");
		name.sendKeys(Keys.ARROW_DOWN);
		name.sendKeys(Keys.ENTER);
		WebElement type=driver.findElement(By.xpath("//*[@name='assignleave[txtLeaveType]']"));
		type.sendKeys(Keys.ARROW_DOWN);
		type.sendKeys(Keys.ARROW_DOWN);
		type.sendKeys(Keys.ARROW_DOWN);
		type.sendKeys(Keys.ARROW_DOWN);
		type.sendKeys(Keys.ENTER);
		type.sendKeys(Keys.ENTER);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


		
		//Date picker
	WebElement selectfromdate=driver.findElement(By.xpath("(//img[@class='ui-datepicker-trigger'])[1]"));
	selectfromdate.click();
//		driver.findElement(By.xpath("(//a[@class='ui-state-default'])[14]")).click();
//        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

List<WebElement> allDates=driver.findElements(By.xpath("//table[@class='ui-datepicker-calendar']//td"));
		
		for(WebElement ele:allDates)
		{
			
		String date=ele.getText();
			
			if(date.equalsIgnoreCase("20"))
			{
				ele.click();
				break;
			}
			
		}
		driver.findElement(By.name("assignleave[txtComment]")).sendKeys("Pongal");
		Thread.sleep(2000);

		WebElement selecttodate=driver.findElement(By.xpath("(//*[@class='ui-datepicker-trigger'])[2]"));
		selecttodate.click();

List<WebElement> allDate=driver.findElements(By.xpath("//table[@class='ui-datepicker-calendar']//td"));
		
		for(WebElement elem:allDate)
		{
			
		String date=elem.getText();
			
			if(date.equalsIgnoreCase("24"))
			{
				elem.click();
				break;
			}
			
		}
		
		driver.findElement(By.name("assignleave[txtComment]")).sendKeys(" Holidays");
		Thread.sleep(1000);
		WebElement Bal=driver.findElement(By.xpath("//*[@id='assignleave_leaveBalance']"));
		System.out.println(Bal.getText());
		JavascriptExecutor js = (JavascriptExecutor) driver;  
		   js.executeScript("(window.scrollBy(0,500))");
		//Assign
		WebElement assign=driver.findElement(By.xpath("//input[@id='assignBtn']"));
		assign.click();
		WebElement orange=driver.findElement(By.id("confirmOkButton"));
		orange.click();

		System.out.println(driver.findElement(By.xpath("//div[@id='content']//h1")).getText());
		System.out.println(driver.findElement(By.xpath("(//div[@class='inner'])[1]")).getText());

	}
	

}
