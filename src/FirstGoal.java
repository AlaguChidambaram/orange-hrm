import java.io.File;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FirstGoal {


	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		// URL
		System.setProperty("webdriver.chrome.driver", "C:\\Work\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://newtours.demoaut.com");
		driver.manage().window().maximize();
		//Thread.sleep(1000);
        WebDriverWait wait = new WebDriverWait(driver,30);
        
		// Register
        WebElement reg = wait.until(ExpectedConditions.visibilityOfElementLocated((By.linkText("REGISTER"))));
        reg.click();

		// Contact Information
		WebElement register = driver.findElement(By.name("firstName"));
		register.sendKeys("xxx");
		driver.findElement(By.name("lastName")).sendKeys("yyy");
		driver.findElement(By.name("phone")).sendKeys("9999999999");
		driver.findElement(By.id("userName")).sendKeys("aaa@gmail.com");
		//Thread.sleep(1000);

		// Mailing information
		driver.findElement(By.name("address1")).sendKeys("A 17");
		driver.findElement(By.name("address2")).sendKeys("Perficient");
		driver.findElement(By.name("city")).sendKeys("Chennai");
		driver.findElement(By.name("state")).sendKeys("tamilnadu");
		driver.findElement(By.name("postalCode")).sendKeys("600000");
		driver.findElement(By.name("country")).click();
		Select select = new Select(driver.findElement(By.name("country")));
		select.selectByValue("92");
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS) ;

	       
		// User Information
		driver.findElement(By.id("email")).sendKeys("aaa");
		driver.findElement(By.name("password")).sendKeys("123455");
		driver.findElement(By.name("confirmPassword")).sendKeys("123455");

		// Submit button
		driver.findElement(By.name("register")).click();
		WebElement elt = driver.findElement(By.xpath("//b[text()=' Dear xxx yyy,']"));
		System.out.println(elt.getText());
		WebElement test = driver.findElement(By.xpath("//font[contains(text(),'Thank you for registering.')]"));
		System.out.println(test.getText());
		WebElement note=driver.findElement(By.xpath("//b[text()=' Note: Your user name is aaa.']"));
		System.out.println(note.getText());

		//System.out.println(driver.findElement(By.xpath("/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td")).getText());
		//System.out.println(driver.findElement(By.xpath("/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td/p[2]/font/a[2]"))
				//.getText());
		//File src= ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		//FileUtils.copyFile(src,new File("D:\\screenshot1.png"));

	}

}
